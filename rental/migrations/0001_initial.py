# Generated by Django 3.2.11 on 2022-01-25 20:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='Category Name')),
            ],
            options={
                'verbose_name': 'Category',
                'verbose_name_plural': 'Categories',
            },
        ),
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, null=True, verbose_name='Name')),
                ('surname', models.CharField(max_length=255, null=True, verbose_name='Surname')),
                ('email', models.EmailField(max_length=254, null=True, verbose_name='Email')),
                ('phone', models.CharField(blank=True, max_length=255, null=True, verbose_name='Phone Number')),
                ('id_number', models.CharField(max_length=255, null=True, verbose_name='ID Number')),
            ],
            options={
                'verbose_name': 'Client',
                'verbose_name_plural': 'Clients',
            },
        ),
        migrations.CreateModel(
            name='Trailer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='Trailer Name')),
                ('vin', models.CharField(max_length=255, verbose_name='VIN Number')),
                ('registration_number', models.CharField(max_length=255, verbose_name='Registration Number')),
            ],
            options={
                'verbose_name': 'Trailer',
                'verbose_name_plural': 'Trailers',
            },
        ),
        migrations.CreateModel(
            name='Booking',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start', models.DateField(verbose_name='Start Date')),
                ('end', models.DateField(verbose_name='Start Date')),
                ('payment_reference', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Payment Reference')),
                ('trailer', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='rental.trailer', verbose_name='Trailer')),
            ],
            options={
                'verbose_name': 'Booking',
                'verbose_name_plural': 'Bookings',
            },
        ),
    ]
