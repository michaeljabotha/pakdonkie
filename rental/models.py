from datetime import timedelta
from django.utils.translation import gettext_lazy as _
from django.db import models
from django.db.models import Q


# Create your models here.
class Booking(models.Model):
    start = models.DateField(_("Start Date"), auto_now=False, auto_now_add=False)
    end = models.DateField(_("End Date"), auto_now=False, auto_now_add=False)
    trailer = models.ForeignKey("rental.Trailer", verbose_name=_("Trailer"), on_delete=models.SET_NULL, null=True, blank=False)
    payment_reference = models.CharField(_("Payment Reference"), max_length=255, null=True, blank=True, default="")
    client = models.ForeignKey("rental.Client", verbose_name=_("Client"), on_delete=models.SET_NULL, null=True, blank=False)
    cost = models.DecimalField(_("Cost"), max_digits=12, decimal_places=2, null=False, blank=False, default=100.00)

    def __str__(self):
        return f"{self.get_reference()}"

    class Meta:
        verbose_name = 'Booking'
        verbose_name_plural = 'Bookings'

    def is_paid(self):
        if self.payment_reference == "":
            return False
        elif self.payment_reference is None:
            return False
        else:
            return True

    def is_valid(self):
        is_valid = True
        if self.start > self.end:
            is_valid = False
        return is_valid

    def get_reference(self):
        return f"PAK-{str(self.id).zfill(5)}"

class Client(models.Model):
    name = models.CharField(_("Name"), max_length=255, null=True, blank=False)
    surname = models.CharField(_("Surname"), max_length=255, null=True, blank=False)
    email = models.EmailField(_("Email"), max_length=254, null=True, blank=False)
    phone = models.CharField(_("Phone Number"), max_length=255, null=True, blank=True)
    id_number = models.CharField(_("ID Number"), max_length=255, null=True, blank=False)

    def __str__(self):
        return f"{self.name} {self.surname}"

    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'

class Trailer(models.Model):

    name = models.CharField(_("Trailer Name"), max_length=50)
    vin = models.CharField(_("VIN Number"), max_length=255)
    registration_number = models.CharField(_("Registration Number"), max_length=255)
    category = models.ForeignKey("rental.Category", verbose_name=_("Trailer Category"), on_delete=models.SET_NULL, null=True, blank=False)
    cost = models.DecimalField(_("Cost"), max_digits=12, decimal_places=2, null=False, blank=False, default=100.00)


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Trailer'
        verbose_name_plural = 'Trailers'

    def is_available_for_booking(self, start, end):
        bookings = Booking.objects.filter(Q(trailer=self) & ( Q(start__lte=end) & Q(end__gte=start)))
        print(bookings)
        bookings = bookings.filter((Q(start__gte=start) & Q(end__lte=end)) | (Q(start__lte=start) & Q(end__gte=start)) | (Q(start__lte=end) & Q(end__gte=end)))
        print(bookings)
        if len(bookings) > 0:
            return False
        else:
            return True

    def get_total_cost(self, start, end):
        delta = end - start 
        print(delta.days)
        return self.cost * delta.days

class Category(models.Model):

    name = models.CharField(_("Category Name"), max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
