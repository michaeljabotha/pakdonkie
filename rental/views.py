import hashlib
import urllib.parse
from datetime import date
from decimal import Decimal
from json import JSONDecoder
import json
from random import choice
from unicodedata import category, name
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from rental.models import Booking, Category, Trailer, Client

# Create your views here.

def generateSignature(dataArray, passPhrase = 'Pakdonkie2024'):
    payload = ""
    for key in dataArray:
        # Get all the data from Payfast and prepare parameter string
        payload += key + "=" + urllib.parse.quote_plus(dataArray[key].replace("+", " ")) + "&"
    # After looping through, cut the last & or append your passphrase
    payload = payload[:-1]
    if passPhrase != '':
        payload += f"&passphrase={passPhrase}"
    return hashlib.md5(payload.encode()).hexdigest()

@csrf_exempt
def get_available_trailer(request):
    post = json.loads(request.body)
    json_data = {
        "trailer": 0,
        "meta": {}
    }
    try:
        start = date(int(post['start'].split("-")[0]), int(post['start'].split("-")[1]), int(post['start'].split("-")[2]))
        end = date(int(post['end'].split("-")[0]), int(post['end'].split("-")[1]), int(post['end'].split("-")[2]))
        category = post['category']
    except:
        return JsonResponse(data=json_data)

    if start < end:
        trailers = Trailer.objects.filter(category__id=category)
        available_trailers = []
        for trailer in trailers:
            if trailer.is_available_for_booking(start, end):
                available_trailers.append(trailer)

        if available_trailers:
            chosen_trailer = choice(available_trailers)
            json_data["trailer"] = chosen_trailer.id
            json_data["meta"] = {
                "cost": f"R{chosen_trailer.get_total_cost(start, end)}",
                "days": (end - start).days,
                "start": post['start'],
                "end": post['end'],
                }

    return JsonResponse(data=json_data)

def make_booking(request):
    data_json = {
        "categories": Category.objects.all(),
    }
    return render(request, 'booking.html', data_json)

@csrf_exempt
def make_booking_submit(request):
    post = json.loads(request.body)
    json_data = {
        "reference": 0,
        "id": 0
    }
    try:
        start = date(int(post['start'].split("-")[0]), int(post['start'].split("-")[1]), int(post['start'].split("-")[2]))
        end = date(int(post['end'].split("-")[0]), int(post['end'].split("-")[1]), int(post['end'].split("-")[2]))
        category = int(post['category'])
        cost = post['cost']
        trailer = int(post['trailer'])

        trailer_obj = Trailer.objects.get(id=trailer)
        category_obj = Category.objects.get(id=category)
        client_obj = None
        if len(Client.objects.filter(id_number=post['id']))>0:
            print("found existing client")
            client_obj = Client.objects.filter(id_number=post['id'])[0]
            client_obj.email = post['email']
            client_obj.phone = post['phone']
            client_obj.save()
        else:
            client_obj = Client()
            client_obj.name = post['name']
            client_obj.surname = post['surname']
            client_obj.id_number = post['id']
            client_obj.email = post['email']
            client_obj.phone = post['phone']
            client_obj.save()
        booking_obj = Booking()
        booking_obj.start = start
        booking_obj.end = end
        booking_obj.trailer = trailer_obj
        booking_obj.client = client_obj
        booking_obj.cost = Decimal(cost[1:])
        booking_obj.save()
    except Exception as e:
        print(e)
        return JsonResponse(data=json_data)

    json_data["reference"] = booking_obj.get_reference()
    json_data["id"] = booking_obj.id
    SANDBOX_MODE = False

    data = {
        # Merchant details
        'merchant_id': '10035469' if SANDBOX_MODE else '25515131',
        'merchant_key': 'slyo1gq2tir59' if SANDBOX_MODE else 'c2y2f0vsptzcn',
        'return_url': 'https://www.pakdonkie.co.za/',
        'cancel_url': 'https://www.pakdonkie.co.za/',
        'notify_url': 'https://www.pakdonkie.co.za/',
        # Buyer details
        'name_first': post['name'],
        'name_last': post['surname'],
        'email_address': post['email'],
        # Transaction details
        'm_payment_id': json_data["reference"],  # Unique payment ID to pass through to notify_url
        'amount': cost[1:],
        'item_name': booking_obj.get_reference()
    }
    print(data)

    signature = generateSignature(data)
    data['signature'] = signature

    # If in testing mode make use of either sandbox.payfast.co.za or www.payfast.co.za
    pfHost = 'sandbox.payfast.co.za' if SANDBOX_MODE else 'www.payfast.co.za'

    htmlForm = f'<form action="https://{pfHost}/eng/process" method="post">'
    for key in data:
        htmlForm += f'<input name="{key}" type="hidden" value="{data[key]}" />'

    htmlForm += '<input type="submit" class="btn btn-info" value="Pay Now" /></form>'

    json_data["form"] = htmlForm
    return JsonResponse(data=json_data)