from django.contrib import admin

from rental.models import Booking, Trailer, Client, Category


class BookingAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'start', 'end', 'cost', 'client')

class TrailerAdmin(admin.ModelAdmin):
    list_display = ('name', 'cost', 'vin', 'registration_number', 'category')

class ClientAdmin(admin.ModelAdmin):
    list_display = ('name', 'surname', 'id', 'email', 'phone')

class CategoryAdmin(admin.ModelAdmin):
    pass

admin.site.register(Booking, BookingAdmin)
admin.site.register(Trailer, TrailerAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Category, CategoryAdmin)