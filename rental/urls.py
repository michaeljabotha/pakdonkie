from django.urls import path

from .views import get_available_trailer, make_booking, make_booking_submit

app_name = "rental"
urlpatterns = [
    path('get_available_trailer/', get_available_trailer, name='get_available_trailer'),
    path('make_booking/', make_booking, name='make_booking'),
    path('make_booking_submit/', make_booking_submit, name='make_booking_submit'),
]   
